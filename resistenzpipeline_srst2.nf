//Pipeline zur Bestimmung von Antibotikaresitenzen aus Fastq dateien mittels srst2

nextflow.enable.dsl = 2

params.infile = "./rawdata/patient*.fastq"
params.outdir = "./out"
params.geneDb = "./CARD_v3.0.8_SRST2.fasta"

process fastp{
    publishDir "${params.outdir}", mode: "copy" , overwrite: true
    container "https://depot.galaxyproject.org/singularity/fastp%3A0.22.0--h2e03b76_0" 
    input:
        path fastq_file
    output:
        path "${fastq_file.getSimpleName()}_trim.fastq", emit: fastq_files
    script:
    """
    fastp -i ${fastq_file} -o ${fastq_file.getSimpleName()}_trim.fastq 
    """
}


process fastqc {

    publishDir "${params.outdir}", mode: "copy" , overwrite: true 
    container "https://depot.galaxyproject.org/singularity/fastqc%3A0.11.9--hdfd78af_1"
    input:
        path fastp_results
    output:
        path "fastqc/*"
    script:
    """
    mkdir fastqc
    fastqc -o fastqc ${fastp_results}
    """
}

process srst2 {

    publishDir "${params.outdir}", mode: "copy", overwrite: true
    container "https://depot.galaxyproject.org/singularity/srst2%3A0.2.0--py27_2"
    
    input:
        path srst2_input
    output:
        path "srst2/*results.txt"//, emit: results_txt 
    script:
    """
    srst2 --input_se ${srst2_input[0]}  --output ./srst2/${srst2_input[0].getSimpleName()}  --log --gene_db ${srst2_input[1]}
    """
}

process results{
    publishDir "${params.outdir}", mode: "copy", overwrite: true
    input: 
        path results_txt
    output:
        path "*"
    script:
    """
    cat ${results_txt} >> results.txt
    """
    
}

workflow {
    
    inchannel = channel.fromPath(params.infile)    
    db_channel = channel.fromPath(params.geneDb)
        
     
    fastp_results = fastp(inchannel)
    fastqc(fastp_results.fastq_files.collect())
    
    srst2_inchannel = fastp_results.fastq_files.combine(db_channel)
    srst2_results = srst2(srst2_inchannel)
    results(srst2_results.collect())
}